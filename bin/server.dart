import 'dart:io';

void main() {
  // Bootstrap application
  new ConnectionListener('0.0.0.0', 4040)..start();
}

class ConnectionListener {
  final dynamic address;
  final int port;
  
  ConnectionListener(this.address, this.port);
  
  void start() {
    HttpServer.bind(address, port)
          .then((HttpServer server) {
              // Log in console to show that server is listening
             print('Server listening on ${address}:${server.port}');
             
             server.listen((HttpRequest request) {
               // Handle incoming connections
             });
          });
  }
}